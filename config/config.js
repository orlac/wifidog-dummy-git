var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';
var _host = 'localhost:3000';
var config_local = require('./config-local.js');
var _ = require('lodash');

var config = {
  development: {
    env: env,
    timeZone: '+03:00',
    host: _host,
    root: rootPath,
    uploadPath: rootPath+'/public/uploads',
    uploadUrl: '/uploads',
    app: {
      name: 'feedman-local'
    },
    port: 3000,
    user: 'root',
    password: '123',
    db: 'mysql://root:123@localhost/feedman-local-development',

    secret: '4546',


    passport: {
      vk: {
        app_id: '',
        secret: '',
        url: 'http://'+_host,
        scopes: ['friends', 'offline', 'wall', 'photos']
      },
      facebook: {
        app_id: '',
        secret: '',
        url: 'http://'+_host,
        scopes: ['user_about_me', 'email', 'publish_actions', 'user_relationship_details', 'user_activities',
                'user_status', 'user_events', 'user_groups', 'user_likes', 'user_education_history',
                'user_birthday', 'user_friends', 'user_hometown', 'user_location', 'user_relationships',
                'user_work_history', 'user_interests', 'user_religion_politics', 'read_stream', 'manage_pages', 'user_photos']
      }
    }
  },

  test: {
    root: rootPath,
    app: {
      name: 'feedman-local'
    },
    port: 3000,
    db: 'mysql://localhost/feedman-local-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'feedman-local'
    },
    port: 3000,
    db: 'mysql://localhost/feedman-local-production'
  }
};

config = _.merge(config, config_local);

module.exports = config[env];
