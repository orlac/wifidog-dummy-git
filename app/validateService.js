var timeZoneService = require('./timeZoneService.js'); 
var async = require('async');
module.exports = {

	isChannelOfUser: function(user_channel_model, user_id_field){
		user_id_field = user_id_field || 'user_id'
		return function(value, next){
			value = value+'';
	        if (value) {
	            var user_id = this.getDataValue('user_id');
	            user_channel_model.find({ where: { id: value, user_id: user_id }}).then(function(m){
	                if (m) {
	                    next()
	                } else {
	                    next('Канал не соответствует пользователю')
	                }
	            }, function(err){
	                next(err.message);
	            });
	        } else {
	            next("Выберите канал");
	        }	
		};
	},

	isTimezone: function(){
		return function (value, next) {
	        if(!value){
	          next();
	        }else{
	          tzList = timeZoneService.getList();
	          async.filter(tzList, function(_item, _cb){
	            _cb(_item.value == value);
	          }, function(results){
	              if(results.length === 0){
	                next('часовой пояс не найден');
	              }else{
	                next();
	              }
	          });
	        }
		}	
	} 
        
}