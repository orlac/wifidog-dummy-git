var validateService = require('../validateService');
var moment = require('moment'); 
var config = require('../../config/config');
var fs = require("fs");
var Promise = require('promise');
var Datauri = require('datauri');
var async = require('async');

var uploadDir = config.root+'/files';
var uploadUrl = '/files';

module.exports = function (sequelize, DataTypes) {

    var user = require('./user.js')(sequelize, DataTypes);
    var user_channel = require('./user_channel.js')(sequelize, DataTypes);

    var getUserDate = function(dateField){
        return function(){
            var _d = this[dateField];
            var tz = this.timeZone || sequelize.options.timezone;
            var d = moment(_d).format('DD.MM.YYYY HH:mm '+sequelize.options.timezone);
            var _date = moment(d, 'DD.MM.YYYY HH:mm Z').zone(tz).format('YYYY-MM-DDTHH:mm:ss Z');
            return _date;
        }
    }
  var model = sequelize.define('user_feeds', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty:{
                    msg: "пользователь не найден"
                }
            }
        },
        chid: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty:{
                    msg: "Выберите канал"
                },
                isChannelOfUser: function (value, next) {
                    validateService.isChannelOfUser(user_channel).apply(this, [value, next]);
                } 
                // isChannelOfUser: validateService.isChannelOfUser(user_channel).bind(this)
            }
        },
        gids: {
            type: DataTypes.TEXT,
            validate: {
                isEven: function(value){
                    value = JSON.parse(value);
                    if( (typeof value !== "object") || Object.keys(value).length === 0 ){
                        throw new Error('Выберите ресурсы для постинга')
                    }
                },
                // check_gids: function(value, next){
                //     user_channel.find({where: {user_id: user.id, id: cid }}).then(function(ush){
                //         if(!ush){
                //             throw  new Error('Канал не найден');            
                //         }
                //         for(var i in gids){
                //             var find = _.findWhere(ush.groups, {id: gids[i].id, type: gids[i].type});
                //             if(!find){
                //                 throw new Error('Не верный ресурс для постинга (id: '+gids.id+')');
                //             }
                //         }
                //         return _save(); 
                //     })
                // }
            },
            // get: function(){
            //     var value = this.getDataValue('gids');
            //     try{
            //         return JSON.parse(value);
            //     }catch(e){
            //         return [];
            //     }
            // },
            // set: function(groups){
            //     this.setDataValue('gids', JSON.stringify(groups) );
            // }
        },
        message: {
            type: DataTypes.TEXT,
            validate: {
                notEmpty:{
                    msg: "Введите сообщение поста (от 3 до 500 символов)"
                },
                len: [3, 500],
            }
        },

        publicOn: {
            type: DataTypes.DATE,
            defaultValue: null,
        },
        publishedOn: {
            type: DataTypes.DATE,
            defaultValue: null,
        },
        deleteOn: {
            type: DataTypes.DATE,
            defaultValue: null,
        },
        deletedOn: {
            type: DataTypes.DATE,
            defaultValue: null,
        },
        createdAt: {
            type: DataTypes.DATE
        },
        updatedAt: {
            type: DataTypes.DATE
        },
        timeZone: {
          type: DataTypes.STRING,
          validate: {
              isTimezone: function (value, next) {
                validateService.isTimezone().apply(this, [value, next]);
              } 
          }
        },
        img: {
            type: DataTypes.STRING
        },
        active: { 
          type: DataTypes.BOOLEAN, 
          allowNull: false, 
          defaultValue: false
        },
    
    }, {
        classMethods: {
            applyGetImages: function(list){
                return new Promise(function (resolve, reject) {
                    async.map(list, function(_item, cb){
                        _item.getLoadImgUrl().then(function(url){
                            _item.imgUrl = url;
                            cb(null, _item);
                        }).catch(function(){
                            cb(null, _item);
                        });
                    }, function(err, results){
                        if(err){
                            reject(err);
                        }else{
                            resolve(results)
                        }
                    });
                });
            }
        },
        getterMethods: {

            userPublicOn: function(){
                return getUserDate('publicOn').apply(this, []);    
            },
            userPublishedOn: function(){
                return getUserDate('publishedOn').apply(this, []);        
            }, 
            gids: function(){
                var value = this.getDataValue('gids');
                try{
                    return JSON.parse(value);
                }catch(e){
                    return [];
                }
            },
            imgUrl: function(){
                return this._imgUrl;
            }
        },
        setterMethods: {
            // userPublicOn: function(val){
            //     this._userPublicOn = val;
            // },
            gids: function(groups){
                this.setDataValue('gids', JSON.stringify(groups) );
            },
            imgUrl: function(val){
                this._imgUrl = val;
            }
        },
        instanceMethods: {
            getLoadImgUrl: function(){
                var me = this;
                return new Promise(function (resolve, reject) {
                    if(me.img){
                        fs.exists(config.uploadPath+'/'+me.img, function (exists) {
                            if(exists){
                                resolve('http://'+ config.host +'/'+ config.uploadUrl+'/'+me.img);
                            }else{
                                reject(null);        
                            }
                        });    
                    }else{
                        reject(null);
                    }
                });
            },

            getLoadImgDataUri: function(){
                var me = this;
                return new Promise(function (resolve, reject) {
                    if(me.img){
                        if(me._getLoadImgDataUti){
                            resolve(me._getLoadImgDataUti);
                        }else{
                            fs.exists(config.uploadPath+'/'+me.img, function (exists) {
                                if(exists){
                                    dUri = new Datauri();

                                    dUri.on('encoded', function (content) {
                                        resolve(this.base64); //=> "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAA...";
                                        me._getLoadImgDataUti = this.base64;
                                    });

                                    dUri.on('error', function (content) {
                                        reject(content);
                                    });

                                    dUri.encode(config.uploadPath+'/'+me.img);

                                    //return DataURI(config.uploadPath+'/'+me.img);
                                    // resolve(config.uploadUrl+'/'+me.img);
                                }else{
                                    reject(null);        
                                }
                            });    
                        }
                    }else{
                        reject(null);
                    }
                });
            }
        },
        timestamps: true,
        freezeTableName: true,
        tableName: 'user_feeds'
  });

  return model;
};