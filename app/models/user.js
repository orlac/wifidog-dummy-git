
//var db = require('../models');


//var user_auth = sequelize.import(__dirname + "/user_auth");
var _ = require('lodash');
var async = require('async');
var validateService = require('../validateService');

module.exports = function (sequelize, DataTypes) {

  var timeZoneService = require('../timeZoneService.js'); 

  var users = sequelize.define('user', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
    createdAt: {
        type: DataTypes.DATE
      },
    updatedAt: {
        type: DataTypes.DATE
      },
    active: { 
      type: DataTypes.BOOLEAN, 
      allowNull: false, 
      defaultValue: true
    },
    timeZone: {
      type: DataTypes.STRING,
      validate: {
          isTimezone: function (value, next) {
            validateService.isTimezone().apply(this, [value, next]);
          } 
      }
    }
    
  }, {
    classMethods: {
      //createOrUpdateUser: _createOrUpdateUser,
      associate: function (models) {
        // example on how to add relations
        // Article.hasMany(models.Comments);
      }
    },

    instanceMethods: {

      // getServices: function(){
      //   return user_auth.findAll({where: {user_id: this.id}})
      // }
    }
  });

  return users;
};