module.exports = function (sequelize, DataTypes) {

  var model = sequelize.define('user_channel', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: DataTypes.INTEGER
        },
        name: {
            type: DataTypes.STRING,
            validate: {
                isEven: function(value){
                    if(!value || value.length > 250){
                        throw new Error('Неверный формат названия!')
                    }
                }
            }
        },
        groups: {
            type: DataTypes.TEXT,
            validate: {
                isEven: function(value){
                    value = JSON.parse(value);
                    if( (typeof value !== "object") || Object.keys(value).length === 0 ){
                        throw new Error('Добавте группы')
                    }
                }
            },
            get: function(){
                var value = this.getDataValue('groups');
                try{
                    return JSON.parse(value);
                }catch(e){
                    return [];
                }
            },
            set: function(groups){
                this.setDataValue('groups', JSON.stringify(groups) );
            }
        },
        createdAt: {
            type: DataTypes.DATE
        },
        updatedAt: {
            type: DataTypes.DATE
        }
    
    }, {

  });

  return model;
};