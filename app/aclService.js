var express = require('express'),
  db = require('./models');  

module.exports = {


	ensureAccess: function(req, res, next){
		if(!req.user){
			res.send(403, {go: '/login', message: 'Нужно авторизоваться'});
		}else{
			if(!req.user.timeZone){
				res.send(403, {go: '/cabinet/settings', message: 'Укажите свой часовой пояс'});
			}else{
				next();	
			}
			
		}
	},

	ensurePreLoginAccess: function(req, res, next){
		if(!req.user){
			res.send(403, {go: '/login', message: 'Нужно авторизоваться'});
		}else{
			next();	
		}
	},

	isAuth: function(req, res, next){
		if(req.user){
			next();
		}else{
			res.sendStatus(403);
		}
	}


}
