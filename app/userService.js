// config = require('./config/config'),
var VK = require('vksdk');
var Facebook = require('facebook-node-sdk');
//var async = require('async-q');
var async = require('async');
var _ = require('lodash');
var Promise = require('promise');
var moment = require('moment'); 
var timeZoneService = require('./timeZoneService.js'); 

//var moment = require('moment-timezone');


module.exports = function(db, config){
	
	var api = require('./api.js')(config); 

	var vk = api.vk;
	var facebook = api.facebook;


	var services = [
		{
			type: 'vk',
			title: 'Вконтакте',
			authUrl: '/auth/vk',
			deleteUrl: '/auth/vk/delete',
		} , 
		{
			type: 'facebook',
			title: 'Facebook',
			authUrl: '/auth/facebook',
			deleteUrl: '/auth/facebook/delete',
		}
	];

	var s = function(db){
		
		this.getAuthServices = function(){
			return services;
		};

		// this.getUserServiceByUid = function(user_id, type){
		// 	with_token = with_token || 
		// 	return db.user_auth.find({ where: {user_id: user_id} });
		// }

		this.getUserServices = function(user){

			return db.user_auth.findAll({ where: {user_id: user.id} }).then(function(models) {
	          
		        var _data = [];
		        for(var _i in services){
		        	var _o = services[_i];
		        	_o.data = {};
		        	for(var i in models){
		        		if(models[i].type_auth == _o.type){
		        			_o.data = {
				        		type: models[i].type_auth,
				        		profileUrl: models[i].getProfileUrl(),
				        		avatar: models[i].getAvatar()
				        	};	
		        		}
			        }
			        _data.push(_o);
		        }
		         
		        return  _data;

	        });

		}

		this.destroyServiceByUser = function(user, service){
			return db.user_auth.find({ where: {user_id: user.id, type_auth: service} }).then(function(_user_auth) {

			    if(_user_auth){
			        return _user_auth.destroy().then(function(m){
			        	// остались ли сервисы для входа
			            return db.user_auth.find({ where: {user_id: user.id} }).then(function(_user_auth){
			                if(!_user_auth){
			                	//если нет, удалить юзера и выйти
			                	return db.user.destroy({where: {id: user.id}}).then(function(){
			                		return logout = true;	
			                	}); 
			                	
			                }else{
			                	return logout = false;
			                }
			            });

			        });
			    }else{
			    	throw new Error('not found service');
			    }
		  	});
		};

		var _f_filter_vk_groups = function(_item, _cb){
				_cb(_item.can_post == 1 && _item.type == "group");
			}
		var _f_map_vk_groups = function(_item, _cb){
				_item.type='vk';
				_cb(null, _item);
			}
		var _f_filter_fb_groups = function(_item, _cb){
				_cb(_item.administrator);
				//_cb(true); //test
			}
		var _f_map_fb_groups = function(_item, _cb){
				_item.type='facebook';
				//страница юзера
				// todo get info
				if(_item.id < 0){
					_item.info = {};
					_cb(null, _item);		
				}else{
					api.fbRequest(facebook, '/'+_item.id)
						.then(function(response){
							_item.info = response;
							_item.photo_50 = response.icon;
							_cb(null, _item);		
						})
						.catch(function(err){
							_cb(err);
						});	
				}
			}

		var _get_feeds_vk = function(user_auth, cb){

			vk.setToken(user_auth.token);
			vk.setSecureRequests(true);
			api.vkRequest(vk, 'groups.get', {
					user_id: user_auth.uid,
					extended: 1,
					fields: 'can_post'
				})
				.then(function(response){
					async.filter(response.items, _f_filter_vk_groups, function(_items){
						_items.push({
							name: 'Моя стена',
							photo_50: user_auth.getAvatar(),
							id: -user_auth.uid
						});

						async.map(_items, _f_map_vk_groups, function(err, _items){
							cb(err, _items);
						});

					});
					
				})
				.catch(function(err){
					cb(err);
				});
		}

		var _get_feeds_facebook = function(user_auth, cb){
			facebook.setAccessToken(user_auth.token);
			api.fbRequest(facebook, '/'+user_auth.uid+'/groups')
				.then(function(response){
					
					async.filter(response.data, _f_filter_fb_groups, function(_items){
						_items.push({
							name: 'Моя стена',
							photo_50: user_auth.getAvatar(),
							id: -user_auth.uid
						});

						async.map(_items, _f_map_fb_groups, function(err, _items){
							cb(err, _items);
						});
						
					});
				})
				.catch(function(err){
					cb(err);
				});
		}

		this.getUserApiFeeds = function(user){

			return db.user_auth.findAll({where: {user_id: user.id}}).then(function(models){

				return new Promise(function (resolve, reject) {
					async.map(models, 
						function(user_auth, cb){

							switch(user_auth.type_auth){
								case 'vk':
									_get_feeds_vk(user_auth, cb);
								break;
								case 'facebook':
									//cb(null, {});
									_get_feeds_facebook(user_auth, cb);
								break;
							}

						}, 
						function(err, results){
							if(err){
								reject(err);
							}else{
								resolve(results);
							}
						});
				});
				
			});

		}

		this.getUserChannels = function(user){
			return db.user_channel.findAll({where: {user_id: user.id}});
		}

		this.addUserChannel = function(user_id, name, groups){
			return db.user_channel.create({
                user_id: user_id,
                name: name,
                groups: groups
            });
		}



		this.getUserWaitFeed = function(user){
			return db.user_feeds.findAll({
				where: [
					{ user_id: user.id }, 
					'publishedOn is null'
				],
				order: 'publicOn desc'
			}).then(function(list){
				return db.user_feeds.applyGetImages(list);
			});
		}

		this.getUserArchiveFeed = function(user){
			return db.user_feeds.findAll({
				where: [
					{user_id: user.id},
					'publishedOn is not null'
				],
				order: 'publishedOn desc'
			}).then(function(list){
				return db.user_feeds.applyGetImages(list);
			});;
		}

		this.addUserFeed = function(user, data){
			
			var cid = data.cid;
			var gids = data.gids;
			return this.getPublishSettings(user).then( function(settings){
				data.timeZone = user.timeZone;
				var _save_data = {
		                user_id: user.id,
		                chid: cid,
		                gids: gids,
		                message: data.message,
		                timeZone: user.timeZone
		            };

	            //save
				var _save = function(){

					var time = data.selectedTime;
					var _d = [
							data.selectedDate, 
							data.selectedTime, 
							data.timeZone
						].join(' ');
					var publicOn = moment(_d, 'DD.MM.YYYY HH:mm Z');//.zone(data.timeZone);//.format('YYYY-MM-DD');

					//date validate
					var _d = moment(_d, 'DD.MM.YYYY HH:mm Z').format('YYYY/MM/DD HH:mm');
					console.log('_d', _d, settings._minDate, settings._maxDate);
					var sec = moment(_d, 'YYYY-MM-DD HH:mm').zone(config.timeZone).unix();
					var min = moment(settings._minDate, 'YYYY/MM/DD HH:mm').unix();
					var max = moment(settings._maxDate, 'YYYY/MM/DD HH:mm').unix();

					if(sec < min){
						throw  new Error('Дата публикации меньше допустимой');
					}
					if( sec > max ){
						throw  new Error('Дата публикации больше допустимой');
					}

					
					//publicOn = publicOn.zone(config.timeZone);
					_save_data.publicOn = publicOn.format('YYYY-MM-DD HH:mm:ss');

					//delete validate
					if(data.isDelete && data.deleteAfter){
						var sec = parseInt(data.deleteAfter);
						var find = _.findWhere(settings.forDelete, {value: sec});
						if(!find){
							throw  new Error('Дата удаления не соответсвует допустимой');		
						}

						var deleteOn = moment(publicOn.toDate() );
						deleteOn.add(data.deleteAfter, 'milliseconds');
						_save_data.deleteOn = deleteOn.format('YYYY-MM-DD HH:mm:ss');
					}
					console.log('_save_data.publicOn', _save_data.publicOn);
					return db.user_feeds.create(_save_data);
				}

				//check gids
				var _check_gids = function(){
					return db.user_channel.find({where: {user_id: user.id, id: cid }}).then(function(ush){
						if(!ush){
							throw  new Error('Канал не найден');			
						}
						for(var i in gids){
							var find = _.findWhere(ush.groups, {id: gids[i].id, type: gids[i].type});
							if(!find){
								throw new Error('Не верный ресурс для постинга (id: '+gids.id+')');
							}
						}
						return _save(); 
					});	
				}

            	return db.user_feeds.build(_save_data).validate().then(function(err, res){
            		if(err){
            			throw err;
            		}else{
            			return _check_gids();	
            		}
            	});
				
			});

			
		}

		this.deleteUserChannel = function(user_id, id){
			return db.user_channel.find({ where: {user_id: user_id, id: id} }).then(function(_user_channel) {
			    if(_user_channel){
			        return _user_channel.destroy();
			    }else{
			    	throw new Error('channel not found');
			    }
		  	});
		}

		this.getNeedPosts = function(limit){
			limit = limit || 10;
			return db.user_feeds.findAll({
				where: [
					{
						publishedOn: null
						,active: true
					},
					// 'publishedOn is null',
					'publicOn < now()',
				],
				order: 'publicOn asc',
				limit: limit
			});
		}

		this.getNeedDeletePosts = function(limit){
			limit = limit || 10;
			return db.user_feeds.findAll({
				where: [
					// {publishedOn: null},
					// 'publishedOn is null',
					'deleteOn < now()',
					'deleteOn is not null',
					'deletedOn is null',
				],
				order: 'deleteOn asc',
				limit: limit
			});/*.then(function(list){
				var ids = _.map(list, function(_item){
					return _item.id;
				});
				db.user_feeds_posts.findAll()

			});*/
		}

		this.getPublishSettings = function(user){
			//todo часовой пояс юзера
			return new Promise(function (resolve, reject) {
				resolve({
					_minDate: moment().add(1, 'minutes').format('YYYY/MM/DD HH:mm'),
					_maxDate: moment().add(30, 'days').format('YYYY/MM/DD HH:mm'),
					minDate: moment().format('YYYY/MM/DD'),
					maxDate: moment().add(30, 'days').format('YYYY/MM/DD'),
					timeZoneList: timeZoneService.getList(),
					defaultTimeZone: (user.timeZone)? user.timeZone: config.timeZone,
					forDelete: [
						{label: '10 минут', value: 10*60*1000}
						,{label: '20 минут', value: 20*60*1000}
						,{label: '40 минут', value: 40*60*1000}
						,{label: '1 час', value: 60*60*1000}
						,{label: '2 часа', value: 2*60*60*1000}
						,{label: '4 часа', value: 4*60*60*1000}
						,{label: '8 часов', value: 8*60*60*1000}
						,{label: '12 часов', value: 12*60*60*1000}
						,{label: 'сутки', value: 24*60*60*1000}
						,{label: '2 суткок', value: 2*24*60*60*1000}
						,{label: 'неделю', value: 7*24*60*60*1000}
					],
			    });
			});
		}

		this.saveUserSettings = function(user, data){
			return db.user.update(data, 
			{
				where: { id : user.id }
			});
		}

		this.toggleUserFeed = function(user, id){
			return db.user_feeds.find({ where: {user_id: user.id, id: id} }).then(function(model) {
			    if(model){
			    	model.active = !model.active;
			    	return model.save();
			    }else{
			    	throw new Error('post not found');
			    }
		  	});
		}

		this.deleteUserFeed = function(user, id){
			return db.user_feeds.find({ where: {
					
					user_id: user.id, 
					id: id,
					publishedOn: null

				} }).then(function(model) {
			    if(model){
			    	return model.destroy();
			    }else{
			    	throw new Error('post not found');
			    }
		  	});
		}
	};

	return new s(db);	
};