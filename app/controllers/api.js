var express = require('express'),
  router = express.Router(),
  acl = require('../aclService'),
  db = require('../models');
var config = require('../../config/config');  

 
var userService = require('../userService')(db, config);  
var sendErrorService = require('../sendErrorService');
var uploadService = require('../uploadService');


module.exports = function (app) {
  app.use('/api', router);
};

router.get('/user/get', function(req, res, next){
      if(req.query.tz_i){
        acl.ensurePreLoginAccess(req, res, next);
      }else{
        acl.ensureAccess(req, res, next)  
      }
      
  }, function (req, res, next) {

  db.user.find({ where: {id: req.user.id} }).then(function(_user) {
    userService.getUserServices(_user).then(function(data){
      //_user.services = data;
        res.send({
          user: _user,
          services: data
        });
    }).catch(function(err){
       sendErrorService.send(res, 500, err);
    });
  }).catch(function(err){
     sendErrorService.send(res, 500, err);
  });
  
});

router.get('/user/feeds/get', acl.ensureAccess,  function (req, res, next) {

  userService.getUserApiFeeds(req.user).then(function(feeds) {
    res.send({
      feeds: feeds,
    });
  }).catch(function(err){
     sendErrorService.send(res, 500, err);
  });
  
});

router.post('/user/channels/add', acl.ensureAccess,  function (req, res, next) {

  var name = req.body.name;
  var groups = req.body.groups;

  userService.addUserChannel(req.user.id, name, groups).then(function(channel) {
    res.send({
      message: 'сохранено',
    });
  }).catch(function(err){
     sendErrorService.send(res, 500, err);
  });
  
});

router.delete('/user/channels/delete', acl.ensureAccess,  function (req, res, next) {

  var id = req.body.id;

  userService.deleteUserChannel(req.user.id, id).then(function(channel) {
    res.send({
      message: 'Удалено',
    });
  }).catch(function(err){
     sendErrorService.send(res, 500, err);
  });
});

router.get('/user/channels/get', acl.ensureAccess,  function (req, res, next) {

  userService.getUserChannels(req.user).then(function(channels) {
    res.send({
      channels: channels,
    });
  }).catch(function(err){
     sendErrorService.send(res, 500, err);
  });
  
});

router.get('/user/publish/settings', acl.ensurePreLoginAccess,  function (req, res, next) {

  userService.getPublishSettings(req.user).then(function(settings) {
    res.send(settings);
  }).catch(function(err){
     sendErrorService.send(res, 500, err);
  });
});

router.post('/user/publish/add', acl.ensureAccess,  function (req, res, next) {
  
  var data = req.body || {};
  // var cid = parseInt(req.body.cid);
  // var gids = req.body.gids;
  return userService.addUserFeed(req.user, data.feed || {}).then(function(obj){
      return res.send({
          id: obj.id,
          message: 'Опубликовано',
      });
  }).catch(function(err){
      return sendErrorService.send(res, 500, err);
  });
  
});

router.post('/user/publish/image/:id', acl.ensureAccess,  function (req, res, next) {
  
  // console.log('req.files', req.params, req.query, req.files, req.body);
  db.user_feeds.find({where: {id: req.params.id, user_id: req.user.id}}).then(function(feed){

    if(!feed){
      throw new Error('feed not found');
    }

    ups = new uploadService.getService({ 
        uploadPath: config.uploadPath,
        fileName: feed.id 
    });

    return ups.run(req).then(function(file){
      return db.user_feeds.update({
        img: file.name
      }, { 
        where: {id: feed.id} 
      } ).then(function(_feed){
        return res.send({
            //file: file,
            message: 'Опубликовано',
        });
      });
    });

  }).catch(function(err){
      return sendErrorService.send(res, 500, err);
  });
  
});

router.get('/user/publish/wait', acl.ensureAccess,  function (req, res, next) {

  userService.getUserWaitFeed(req.user).then(function(list) {
    res.send({
      data: list,
    });
  }).catch(function(err){
    sendErrorService.send(res, 500, err);
  });
});

router.get('/user/publish/archive', acl.ensureAccess,  function (req, res, next) {
  userService.getUserArchiveFeed(req.user).then(function(list) {
    res.send({
      data: list,
    });
  }).catch(function(err){
    sendErrorService.send(res, 500, err);
  });
});

router.post('/user/settings', acl.ensurePreLoginAccess,  function (req, res, next) {

  var tz = req.body.timeZone;
  userService.saveUserSettings(req.user, {timeZone: tz}).then(function(model) {
    res.send({
      message: 'сохранено',
    });
  }).catch(function(err){
    sendErrorService.send(res, 500, err);
  });
  
});

router.post('/user/publish/toggle', acl.ensureAccess,  function (req, res, next) {

  var id = req.body.id;
  userService.toggleUserFeed(req.user, id).then(function(model) {
    res.send({
      message: 'сохранено',
    });
  }).catch(function(err){
    sendErrorService.send(res, 500, err);
  });
  
});

router.delete('/user/publish/delete', acl.ensureAccess,  function (req, res, next) {

  var id = req.body.id;
  userService.deleteUserFeed(req.user, id).then(function(model) {
    res.send({
      message: 'удалено',
    });
  }).catch(function(err){
    sendErrorService.send(res, 500, err);
  });
  
});

router.get('/keys/get', function (req, res, next) {

  // var keys = [];
  // for(var service in userService.getAuthServices()){
  //     keys.push({
  //       type: service,
  //       authUrl: '/auth/'+service
  //     });
  // }
  res.send(userService.getAuthServices());

});




