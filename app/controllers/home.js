var express = require('express'),
  router = express.Router(),
  //marko = require('marko'),
  db = require('../models');

module.exports = function (app) {
  app.use('/', router);
};

//var indexTemplate = marko.load(require.resolve('../views/index.marko'));
router.get('/', function (req, res, next) {

    res.render('index', {
      title: '',
      date: new Date().toUTCString()
    });
    // indexTemplate.render({
    //   date: new Date().toUTCString()
    // }, res);
  // db.Article.findAll().success(function (articles) {
  //   indexTemplate.render({
  //     title: 'Generator-Express MVC',
  //     articles: articles,
  //     date: new Date().toUTCString()
  //   }, res);
  // });

  // db.user.create({id: 1, active: true})
  //   .then(
  //     function(model){
  //       console.log('success', model);
  //       res.send(model);
  //     }
  //     ,function(error){
  //       console.log('error', error);
  //       res.send(error);
  //     });

});
