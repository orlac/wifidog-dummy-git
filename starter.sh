#!/bin/sh

# NAME="starter-`date '+%F-%H-%M-%S'`"
LOG="starter.log"

ERROR_LOG="starter.error.log"
OUT_LOG="starter.out.log"

POSTER_ERROR_LOG="poster.error.log"
POSTER_OUT_LOG="poster.out.log"

DELETER_ERROR_LOG="deleter.error.log"
DELETER_OUT_LOG="deleter.out.log"

LOG_PATH="/home/bragin/feedman/logs/"
APP_PATH="/home/bragin/feedman"

FULL=$MY_PATH$NAME

echo $FULL

grunt build
export PATH=/usr/local/bin:$PATH

pm2 stop all

kill `pidof grunt`

# pm2 stop mynode_app
pm2 start $APP_PATH/app.js --name mynode_app  -i max -e $LOG_PATH$ERROR_LOG -o $LOG_PATH$OUT_LOG

# pm2 stop poster_app
pm2 start $APP_PATH/poster.js --name poster_app  -i max -e $LOG_PATH$POSTER_ERROR_LOG -o $LOG_PATH$POSTER_OUT_LOG

# pm2 stop deleter_app
pm2 start $APP_PATH/deleter.js --name deleter_app  -i max -e $LOG_PATH$DELETER_ERROR_LOG -o $LOG_PATH$DELETER_OUT_LOG

# if [ $(ps -e -o uid,cmd | grep $UID | grep node | grep -v grep | wc -l | tr -s "\n") -eq 0 ]
# then
#         export PATH=/usr/local/bin:$PATH

#         # pm2 start $APP_PATH gruntstart.js --name mynodeapp  -i max --log-date-format "YYYY-MM-DD" -e $LOG_PATH$ERROR_LOG -o $LOG_PATH$OUT_LOG
#         # forever start -m 1 --sourceDir /home/bragin/feedman gruntstart.js > $MY_PATH$NAME 2>&1
# fi