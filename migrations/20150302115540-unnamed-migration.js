"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.addColumn(
	  'users',
	  'timeZone',
	  DataTypes.STRING
	).complete(done);
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.dropColumn(
	  'users',
	  'timeZone'
	).complete(done);
  }
};
