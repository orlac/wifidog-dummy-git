"use strict";

var _tbl = 'users';
module.exports = {
  up: function(migration, DataTypes, done) {
    migration.createTable(
	  _tbl,
	  {
	    id: {
	      type: DataTypes.INTEGER,
	      primaryKey: true,
	      autoIncrement: true
	    },
	    createdAt: {
	      type: DataTypes.DATE
	    },
	    updatedAt: {
	      type: DataTypes.DATE
	    },
	    active: {
	      type: DataTypes.BOOLEAN,
	      defaultValue: true,
	      allowNull: false
	    }
	  }
	).complete(done);
    //done();
  },

  down: function(migration, DataTypes, done) {
  	migration.dropTable(_tbl).complete(done);
    // add reverting commands here, calling 'done' when finished
    //done();
  }
};
