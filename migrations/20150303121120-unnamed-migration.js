"use strict";

module.exports = {
  up: function(migration, DataTypes, done) {
    // add altering commands here, calling 'done' when finished
    migration.addColumn(
	  'user_feeds',
	  'img',
	  DataTypes.STRING
	).complete(done);
  },

  down: function(migration, DataTypes, done) {
    // add reverting commands here, calling 'done' when finished
    migration.dropColumn(
	  'user_feeds',
	  'img'
	).complete(done);
  }
};
