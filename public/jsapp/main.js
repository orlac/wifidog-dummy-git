(function(w){
	var t = require('../components/jquery/dist/jquery.min.js');
	w.$ = t;
	w.jQuery = t;
	require('../components/bootstrap/dist/js/bootstrap.min.js');
	w.moment = require('../components/moment/moment.js');
	require('../js/calendar.min.js');
	require('../js/datetimepicker.min.js');
	var _ = require('../components/lodash/lodash.min.js');
	w._ = _;

	//elements
	require('../elements/header-menu/script.js');
	//require('../elements/apiService.js');

})(window);

