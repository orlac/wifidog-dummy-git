(function(scope){
    'use strict';
    console.log('file', 'apiService.js');

    // var err_dialog;
    // var _error = function(error){
    //    //err_dialog.heading="Какая-то непонятная ошибка";
    //    err_dialog.text = 'Ошибочка вышла';
    //    err_dialog.innerHTML = error;
    //    err_dialog.show();
    // }

    // var __error = function(err){
    //   _error( err.message || 'что-то пошло не так...' );
    // }


    Polymer('api-service', {
      
      errorText: '',
      ready: function() {
        console.log('ready', scope);
        this.xhr = /*this.$.xhr; //*/document.createElement('core-xhr');
        //err_dialog = this.$.errAst;// document.createElement('paper-toast');
        //document.body.appendChild(err_dialog);
      },
      
      publish: {

          ensureAccessNotLogin: function(){
            var me = this;
            this.getUser(function(){
                me.go('/cabinet/keys')
            }, function(){});  
          },
          ensureAccess: function(ignoreTimeZone){
            var me = this;
            var data = {};
            if(ignoreTimeZone){
              data.tz_i = true;
            }
            this._get_user(data, null, function(err){ 
                me.go( err.go || '/')
                me.$.notify.error( err.message || 'Доступ запрещен' )
            });  
          },

          _get_user: function(data, _ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/get',
              data: data,
              success: function(res){
                console.log('/api/user/get', res);
                if(_ok){
                  _ok(res);
                }
              },
              error: function(err){
                err = err.responseJSON || {};
                console.error('/api/user/get ', err);
                if(_err){
                  _err(err);
                }else{
                  me.$.notify.error( err.message || 'что-то пошло не так...' )
                }
              }
            });
          },

          getUser: function(_ok, _err){
            var me = this;
            me._get_user(null, _ok, _err);
          },

          getAuthServices: function(_ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/keys/get',
              success: _ok,
              error: function(err){
                if(_err){
                  _err(err);
                }else{
                  me.$.notify.error( err.message || 'что-то пошло не так...' );
                }
              }
            });
          },

          getUserFeeds: function(_ok){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/feeds/get',
              success: _ok,
              error: me.$.notify.error
            });
          },

          getUserChannels: function(_ok){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/channels/get',
              success: _ok,
              error: me.$.notify.error
            });
          },

          deleteUserChannel: function(id, _ok){
            var me = this;
            $.ajax({
              type: 'DELETE',
              dataType: 'json',
              data: {
                id: id
              },
              url: '/api/user/channels/delete',
              success: _ok,
              error: me.$.notify.error
            });
          },

          addFeed: function(data, _ok){
            var me = this;
            $.ajax({
              type: 'post',
              data: data,
              dataType: 'json',
              url: '/api/user/channels/add',
              success: _ok,
              error: me.$.notify.error
            });
          },

          saveUserFeed: function(files, data, _ok, _err){
            var me = this;
            
            // for(var i in data){
            //   formData.append(i, data[i]);  
            // }
            // formData.append('id', '123');
            // formData.append('form', JSON.stringify(data));
            // if(files.length > 0){
            //   formData.append('uploadFile', files[0]);
            // }
            
            $.ajax({
              type: 'POST',
              data: {feed: data},
              cache: false,
              dataType: 'json',
              url: '/api/user/publish/add',
              success: function(res){
                if(files.length > 0){
                  var formData = new FormData();
                  formData.append('uploadFile', files[0]);
                  $.ajax({
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'json',
                    url: '/api/user/publish/image/'+res.id,
                    success: _ok,
                    error: _err || me.$.notify.error
                  });      
                }else{
                  _ok(res);
                }
              },
              error: _err || me.$.notify.error
            });
          },

          getUserWaitFeed: function(_ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/publish/wait',
              success: _ok,
              error: _err || me.$.notify.error
            });
          },

          getUserArchiveFeed: function(_ok, _err){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/publish/archive',
              success: _ok,
              error: _err || me.$.notify.error
            });
          },

          getPublishSettings: function(_ok){
            var me = this;
            $.ajax({
              dataType: 'json',
              url: '/api/user/publish/settings',
              success: _ok,
              error: me.$.notify.error
            });
          },

          saveSettings: function(data, _ok){
            var me = this;
            $.ajax({
              type: 'post',
              data: data,
              dataType: 'json',
              url: '/api/user/settings',
              success: _ok,
              error: me.$.notify.error
            });
          },

          toggleUserFeed: function(id, _ok){
            var me = this;
            $.ajax({
              type: 'post',
              data: {id: id},
              dataType: 'json',
              url: '/api/user/publish/toggle',
              success: _ok,
              error: me.$.notify.error
            });
          },

          deleteUserFeed: function(id, _ok){
            var me = this;
            $.ajax({
              type: 'DELETE',
              data: {id: id},
              dataType: 'json',
              url: '/api/user/publish/delete',
              success: _ok,
              error: me.$.notify.error
            });
          },

          go: function(path){
            //todo костыльно это
            scope.document.getElementById('router').go(path, {replace: true})
          }
      },
      /**
       * The `task` method does no work at this time.
       *
       * @method task
       * @return {Object} Returns undefined.
       * @param {String} dummy Serves no purpose today.
       */
      task: function(dummy) {
        return dummy;
      }

    });

})(window);